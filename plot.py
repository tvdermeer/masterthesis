### importeer een paar packages die nodig zijn ###
import pandas as pd 
from bokeh.plotting import figure, show, output_file, ColumnDataSource
from bokeh.models import LabelSet, Label

### data inladen, dus vul hieronder het pad naar het bestand in ### 
data = pd.read_pickle('hier komt een pad') #### voorbeeld 'data/pca_joris'

### voorbeeldje van hoe de data eruit ziet ###
data.head()

### het maken van de plot ###
p = figure(title = 'interesses onderzoek Joris')

p.circle(data['x'], data['y'])

### maken van de labels en die toevoegen ###
labels = LabelSet(x='x', y='y', text='intresse', level='glyph',
              x_offset=5, y_offset=5, text_font_size = '8pt',
            source= ColumnDataSource(data), render_mode='canvas')

output_file('interesses_Joris.html', title='interesses Joris')

p.add_layout(labels)

show(p)

################## einde van het document #######################