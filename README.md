# masterthesis

to install and run, follow the following steps 

1. 'clone' or 'download' the files onto your computer

2. open the files with Visual studio code 

3. open the terminal and `cd` to the folder

4. open a virtual env and run the following command `pip -r requirements.txt`

this will install the packages needed to run the code

5. set the path to the right data you want to analyse

6. run the code, it should work well

7. extra little debugging added where `.show()` is not really needed for the code to execute

Good luck!
